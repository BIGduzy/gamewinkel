-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 12 nov 2015 om 23:15
-- Serverversie: 5.6.17
-- PHP-versie: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `gamewinkel`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `digital_stock`
--

CREATE TABLE IF NOT EXISTS `digital_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_digital_product_idx` (`product`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Gegevens worden geëxporteerd voor tabel `digital_stock`
--

INSERT INTO `digital_stock` (`id`, `product`, `amount`) VALUES
(1, 1, 9),
(2, 2, 9),
(3, 5, 10),
(4, 6, 10),
(5, 7, 10),
(6, 8, 10),
(7, 9, 10),
(8, 10, 10),
(9, 11, 10),
(10, 12, 10),
(11, 13, 8),
(12, 14, 9),
(13, 15, 8),
(14, 16, 9),
(15, 17, 10),
(16, 18, 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0 = Waiting for pickup, 1 = done, 2 = pre order ready',
  `order_date` date DEFAULT NULL,
  `preorder` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = false 1 = true',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Gegevens worden geëxporteerd voor tabel `order`
--

INSERT INTO `order` (`id`, `user`, `status`, `order_date`, `preorder`) VALUES
(20, 1, 0, '2015-11-12', 0),
(21, 1, 0, '2015-11-12', 1),
(22, 1, 0, '2015-11-12', 0),
(23, 1, 0, '2015-11-12', 1),
(24, 1, 0, '2015-11-12', 0),
(25, 1, 0, '2015-11-12', 1),
(26, 1, 0, '2015-11-12', 0),
(27, 1, 0, '2015-11-12', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `order_product_junction`
--

CREATE TABLE IF NOT EXISTS `order_product_junction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `platform_id` (`platform_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Gegevens worden geëxporteerd voor tabel `order_product_junction`
--

INSERT INTO `order_product_junction` (`id`, `order_id`, `product_id`, `platform_id`) VALUES
(21, 21, 14, 2),
(22, 21, 15, 3),
(23, 23, 18, 1),
(24, 23, 18, 1),
(25, 25, 18, 1),
(26, 25, 16, 4),
(27, 27, 18, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `physical_stock`
--

CREATE TABLE IF NOT EXISTS `physical_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_physical_product_idx` (`product`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Gegevens worden geëxporteerd voor tabel `physical_stock`
--

INSERT INTO `physical_stock` (`id`, `product`, `amount`) VALUES
(1, 1, 10),
(2, 2, 10),
(3, 5, 10),
(4, 6, 10),
(5, 7, 10),
(6, 8, 10),
(7, 9, 10),
(8, 10, 10),
(9, 11, 10),
(10, 12, 10),
(11, 13, 10),
(12, 14, 10),
(13, 15, 10),
(14, 16, 10),
(15, 17, 10),
(16, 18, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `platform`
--

CREATE TABLE IF NOT EXISTS `platform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden geëxporteerd voor tabel `platform`
--

INSERT INTO `platform` (`id`, `name`) VALUES
(1, 'Xbox one'),
(2, 'Playstation 4'),
(3, 'Pc'),
(4, 'Wii u');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `platform_product_junction`
--

CREATE TABLE IF NOT EXISTS `platform_product_junction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_platform_product_idx` (`platform_id`),
  KEY `fk_product_id_idx` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Gegevens worden geëxporteerd voor tabel `platform_product_junction`
--

INSERT INTO `platform_product_junction` (`id`, `platform_id`, `product_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2),
(4, 4, 2),
(5, 1, 5),
(6, 2, 6),
(7, 3, 7),
(8, 4, 8),
(9, 1, 8),
(10, 2, 9),
(11, 3, 9),
(12, 1, 11),
(13, 4, 12),
(14, 3, 12),
(15, 4, 10),
(16, 3, 10),
(17, 2, 10),
(18, 1, 10),
(19, 1, 13),
(20, 2, 14),
(21, 3, 15),
(22, 4, 16),
(23, 4, 17),
(24, 1, 18);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `releasedate` date NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `youtube` varchar(45) DEFAULT NULL,
  `sound` varchar(255) DEFAULT NULL,
  `screenshot` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Gegevens worden geëxporteerd voor tabel `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `price`, `releasedate`, `image`, `youtube`, `sound`, `screenshot`) VALUES
(1, 'Onikira', 'Onikira is a 2D side scrolling beat ‘em up set in a fantasy feudal Japan where the player takes on the role of a samurai fighting to prevent the forces of the Japanese underworld from breaking through into the world of the living.', 13.99, '2015-08-27', 'onikira/onikira.png', '8OH31zfRlDs', NULL, 'onikira/onikira-screenshot.png'),
(2, 'DYNASTY WARRIORS 8 Empires', 'DYNASTY WARRIORS 8 Empires is de nieuwste game uit de Empires-reeks. Probeer het oude China te veroveren via strategie en door het met één krijger tactisch op te nemen tegen duizenden tegenstanders, gameplay waar de WARRIORS-reeks zijn faam aan dankt.', 49.99, '2015-02-26', 'dynasty-warriors-8-empires/dynasty-warriors-8-empires.jpg', '5fFoepNE6b4', 'dynasty-warriors-8-empires/dynasty-warriors-8-empires.mp3', NULL),
(5, 'Hero Siege', 'Hero Siege is a Hack ''n'' Slash game with roguelike- & RPG elements. Annihilate hordes of enemies, grow your talent tree, grind better loot and explore up to 6 Acts enhanced with beautiful Pixel Art graphics! This game offers countless hours of gameplay and up to 4 player online multiplayer!', 1.49, '2012-01-29', 'hero-siege/hero-siege.jpg', 'IvDyKN9ROGU', 'hero-siege/hero-siege.mp3', NULL),
(6, 'Grim Dawn', 'Enter an apocalyptic fantasy world where humanity is on the brink of extinction, iron is valued above gold and trust is hard earned. This ARPG features complex character development, hundreds of unique items, crafting and quests with choice & consequence.', 24.99, '2013-11-05', NULL, 'GU1ViHjUJF8', NULL, NULL),
(7, 'FINAL FANTASY TYPE-0 HD', 'FINAL FANTASY TYPE-0 HD brings an immersive world, memorable characters and the production value known of the series, together with high quality gameplay and storytelling truly worthy of the FINAL FANTASY name.', 24.99, '2015-08-08', 'final-fantasy-type-0-hd/final-fantasy-type-0-hd.png', NULL, 'final-fantasy-type-0-hd/final-fantasy-type-0-hd.mp3', NULL),
(8, 'Torchlight II', 'De bekroonde actie-RPG is terug, groter en beter dan ooit! Torchlight II neemt je terug naar een eigenzinnige wereld met een hoog tempo, vol bloeddorstige monsters, overvloedige schatten en sinistere geheimen - en weer ligt het lot van de wereld in jouw handen!', 18.99, '2012-09-20', 'torchlight-ii/torchlight-ii.png', NULL, NULL, NULL),
(9, 'Chivalry: Medieval Warfare', 'Beleger kastelen en plunder dorpen in Chivalry: Medieval Warfare, een razendsnelle, middeleeuwse first-person slasher met een focus op multiplayergevechten.', 22.99, '2012-10-16', 'chivalry-medieval-warfare/chivalry-medieval-warfare.jpg', NULL, NULL, NULL),
(10, 'Mount & Blade: Warband', 'In a land torn asunder by incessant warfare, it is time to assemble your own band of hardened warriors and enter the fray. Lead your men into battle, expand your realm, and claim the ultimate prize: the throne of Calradia!', 19.99, '2010-03-31', 'mount-blade-warband/mount-blade-warband.jpg', NULL, NULL, NULL),
(11, 'Castle Crashers', 'Baan je hakkend, slaand en beukend een weg naar de overwinning in dit bekroonde 2d-arcadeavontuur van The Behemoth!', 11.99, '2012-09-26', 'castle-crashers/castle-crashers.jpg', NULL, NULL, 'castle-crashers/castle-crashers-screenshots.jpg'),
(12, 'Hammerwatch', 'A hack and slash action adventure, set in a fantasy pixel art environment. Play solo or co-op in this adventure from bottom to top of Castle Hammerwatch. Kill hordes of enemies with varied looks and features through four unique environments with traps, hidden secrets and puzzles.', 8.99, '2013-08-12', 'hammerwatch/hammerwatch.jpg', NULL, NULL, NULL),
(13, 'Onikira 2', 'Onikira 2 is a 2D side scrolling beat ‘em up set in a fantasy feudal Japan where the player takes on the role of a samurai fighting to prevent the forces of the Japanese underworld from breaking through into the world of the living.', 20, '2016-01-13', NULL, '8OH31zfRlDs', NULL, 'onikira/onikira-screenshot.png'),
(14, 'DYNASTY WARRIORS 9 Empires', 'DYNASTY WARRIORS 8 Empires is de nieuwste game uit de Empires-reeks. Probeer het oude China te veroveren via strategie en door het met één krijger tactisch op te nemen tegen duizenden tegenstanders, gameplay waar de WARRIORS-reeks zijn faam aan dankt.', 60, '2016-02-10', NULL, '5fFoepNE6b4', 'dynasty-warriors-8-empires/dynasty-warriors-8-empires.mp3', NULL),
(15, 'Hero Siege 2', 'Hero Siege 2 is a Hack ''n'' Slash game with roguelike- & RPG elements. Annihilate hordes of enemies, grow your talent tree, grind better loot and explore up to 6 Acts enhanced with beautiful Pixel Art graphics! This game offers countless hours of gameplay and up to 4 player online multiplayer!', 20, '2015-12-24', NULL, 'IvDyKN9ROGU', 'hero-siege/hero-siege.mp3', NULL),
(16, 'Torchlight III', 'De bekroonde actie-RPG is terug, groter en beter dan ooit! Torchlight II neemt je terug naar een eigenzinnige wereld met een hoog tempo, vol bloeddorstige monsters, overvloedige schatten en sinistere geheimen - en weer ligt het lot van de wereld in jouw handen!', 30, '2015-12-04', NULL, NULL, NULL, NULL),
(17, 'Chivalry: Medieval Warfare 2', 'Beleger kastelen en plunder dorpen in Chivalry: Medieval Warfare, een razendsnelle, middeleeuwse first-person slasher met een focus op multiplayergevechten.', 20, '2015-11-17', NULL, NULL, NULL, NULL),
(18, 'Castle Crashers 2', 'Baan je hakkend, slaand en beukend een weg naar de overwinning in dit bekroonde 2d-arcadeavontuur van The Behemoth!', 10, '2015-11-11', NULL, NULL, NULL, 'castle-crashers/castle-crashers-screenshots.jpg');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `supply`
--

CREATE TABLE IF NOT EXISTS `supply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Gegevens worden geëxporteerd voor tabel `supply`
--

INSERT INTO `supply` (`id`, `user`, `status`, `date`) VALUES
(1, 2, 0, '2015-09-14'),
(2, 2, 1, '2015-10-31');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `supply_product_junction`
--

CREATE TABLE IF NOT EXISTS `supply_product_junction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supply_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`supply_id`),
  KEY `product_id` (`product_id`),
  KEY `platform_id` (`platform_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden geëxporteerd voor tabel `supply_product_junction`
--

INSERT INTO `supply_product_junction` (`id`, `supply_id`, `product_id`, `platform_id`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1),
(3, 1, 5, 3),
(4, 2, 7, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(145) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `role` tinyint(4) DEFAULT NULL COMMENT '0 = customer, 1 = admin, 2 = salesclerk',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `name`, `address`, `phone`, `role`) VALUES
(1, 'nick-b@live.nl', 'geheim', 'Nick Bout', 'Burg Doormanstraat 3 \n3465KD  DRIEBRUGGEN', '12345234324', 0),
(2, 'admin@gamewinkel.nl', 'geheim', 'admin', 'nvt', '', 1),
(3, 'wouter@live.nl', 'geheim', 'Wouter Dijkstra', 'Elzenlaan 2 3465TJ Driebruggen', '0348502345', 0),
(4, 'f.drostenborg@mailinator.com', 'geheim', 'Michélle Koenraadt', 'Prins Claushof 7 6661 VE Elst Nederland', '0481-524589', 0),
(5, 'r.bretvelt@mailinator.com', 'geheim', 'Rien Bretvelt', 'Johannes Vermeerstraat 19 1071 DK Amsterdam Nederland', '020-8027495', 0),
(6, 'c.baken@mailinator.net', 'geheim', 'Cyril Baken', 'Stieltjesplein 2\r\n3071 JT Rotterdam\r\nNederland', '010-6430720', 0),
(7, 'benniebertram@mailin8r.com', 'geheim', 'Bennie Bertram', 'Gerard Douplein 35\r\n1073 XG Amsterdam\r\nNederland', '020-0286407', 0),
(8, 'verkoper@gamewinkel.nl', 'geheim', 'verkoper', NULL, NULL, 2),
(9, 'petra.bijlert@mailinator.net', 'geheim', 'Petra Bijlert', 'Johannes Verhulststraat 145\r\n1071 NB Amsterdam\r\nNederland', '020-1584666', 0),
(10, 't.delalieverdink@smellfear.com', 'geheim', 'Thierry Dela Lieverdink', '1e Lindendwarsstraat 28\r\n1015 LG Amsterdam\r\nNederland', '020-2214967', 0),
(11, 'blubberbuikje@gmail.com', 'geheim', 'Nick van Buuren', 'Burg. Doormanstraat 3 3465KD Driebruggen', '0348501367', 0);

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `digital_stock`
--
ALTER TABLE `digital_stock`
  ADD CONSTRAINT `fk_digital_product` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `order_product_junction`
--
ALTER TABLE `order_product_junction`
  ADD CONSTRAINT `fk_order_orderproduct` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_platform_orderproduct` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_orderproduct` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `physical_stock`
--
ALTER TABLE `physical_stock`
  ADD CONSTRAINT `fk_physical_product` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `platform_product_junction`
--
ALTER TABLE `platform_product_junction`
  ADD CONSTRAINT `fk_platform_id` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Beperkingen voor tabel `supply_product_junction`
--
ALTER TABLE `supply_product_junction`
  ADD CONSTRAINT `fk_platform_supplyproduct` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_supplyproduct` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_supply_supplyproduct` FOREIGN KEY (`supply_id`) REFERENCES `supply` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
