<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/library/sqlquery.class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/public/lib//PHPMailer/PHPMailerAutoload.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/application/models/product.php');

$sql = new SqlQuery();
$sql->connect(DB_HOST, DB_USER, PASSWORD, DB_NAME);
$rawOrders = $sql->query("SELECT * FROM `order` where `preorder` = 1;");

foreach ($rawOrders as $key => $rawOrder) {
    // Get orders by order id
    $query = "SELECT `product`.*,
                     `platform`.name
              FROM `order_product_junction` AS `oj`
              JOIN `order` AS `order` ON `order`.id = `oj`.order_id
              JOIN `product` AS product ON `product`.id = `oj`.product_id
              JOIN `platform` AS platform ON `platform`.id = `oj`.platform_id
              WHERE `oj`.order_id = {$rawOrder['Order']['id']}
              AND `order`.status = 0;";
    $rawProducts = $sql->query($query);

    $products = [];
    $readyForPickup = true;
    $today = new DateTime(date('Y-m-d'));
    foreach ($rawProducts as $rawProduct) {
        $product = new Product($rawProduct);
        $productDate = new DateTime($product->getReleasedate());

        if ($productDate > $today) {
            $readyForPickup = false;
        }
        
        // array_push($products, $product);
    }

    if ($readyForPickup) {
        $user = $sql->query("SELECT `name`,`email` FROM `user` WHERE `id` = {$rawOrder['Order']['user']};")[0]['User'];
        $dir = $_SERVER['DOCUMENT_ROOT']."/public/pdf/".$rawOrder['Order']['id'].".pdf";
        sendOrderMail($user, $rawOrder['Order']['id'], $dir);
    }
}


/**
*   Send mail for order
*   @param $user Array with email and name
*   @param $orderId The Id of the order
*   @param $dir The directory of the order pdf
*   @return boolean Send mail succes true or false
*
*/
function sendOrderMail($user, $orderId, $dir) {
    $mail = new PHPMailer();

    // Google SMTP
    $mail->IsSMTP(); // telling the class to use SMTP
    $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
    $mail->SMTPAuth = true; // enable SMTP authentication
    $mail->Port = 465; // set the SMTP port for the GMAIL server
    $mail->Username = "samuraaaaaaai@gmail.com"; // GMAIL username
    $mail->Password = "samura*7i"; // GMAIL password
    $mail->SMTPSecure = "ssl"; // sets the prefix to the servier

    $mail->setFrom('GameWinkel@info.nl', 'GameWinkel');   // From
    $mail->addAddress($user['email'], $user['name']);     // Add a recipient
    $mail->addAttachment($dir, 'Bestelling.pdf');           // Add pdf as attachment
    $mail->isHTML(true);                               // Set email format to HTML

    $mail->Subject = 'Bestelling #'.$orderId." kan worden opgehaalt";          // Add subject
    $mail->Body    = 'Vanaf vandaag kun u uw gepreorderde game komen ophalen!, Nogmaal bedankt voor uw bestelling bij GameWinkel <br> Zie de bijlage voor extra informatie over uw bestelling';
    $mail->AltBody = 'Vanaf vandaag kun u uw gepreorderde game komen ophalen!, Nogmaal bedankt voor uw bestelling bij GameWinkel Zie de bijlage voor extra informatie over uw bestelling'; // No html body

    return $mail->send();
}