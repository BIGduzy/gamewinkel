function filter(value) {
    var items = document.getElementsByClassName("item");

    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var platforms = item.className.split(" ");
        var contains = false;

        for (var j = 1; j < platforms.length; j++) { // 1 because we skip 'item' class
            var platform = platforms[j];

            if (platform === value) {
                contains = true;
            }
        }

        if (contains || parseInt(value) === 0) {
            item.style.display = "inline-block";
        } else {
            item.style.display = "none"
        }
    }
}