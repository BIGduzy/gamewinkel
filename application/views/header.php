<!DOCTYPE html>
<html>
	<head>
		<link rel="icon" href="/img/gamewinkel.png">
		<title> GameWinkel </title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
		<link rel='stylesheet' href="/css/style.css" type="text/css" />
    <script src="/js/script.js"></script>
	</head>
	<body>
		<div id="top-bar">
      <nav>
        <a href="/">home</a>
      </nav>
    </div>
    
    <div id="container">
      <header>
        <form method="POST" action="/users/products"> <input type="text" name="search" class="searchbar" placeholder="Zoeken"></form>
      </header>
      <a href="/"> <img src="/img/gamewinkel.png"> </a>
      <nav>
        <?php include("link.php"); ?>
      </nav>
      <content>
      