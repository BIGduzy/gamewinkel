<?php
if (isset($message)) {
    echo $message;
} else {
?>

<div class="cart">

    <div class="cart-row">
        <div class="cart-product">
            <?php echo "Order #".$orderId ?>
        </div>
        <div class="cart-platform">
            <?php echo "Status ".$status ?>
        </div>
        <div class="cart-amount">
            <?php echo "Bestellings datum ".$date ?>
        </div>
        <div class="cart-user">
            <?php echo "Gebruiker:<br>".$user ?>
        </div>
    </div>
    <div class="cart-row">
        <div class="cart-product">
            Product
        </div>
        <div class="cart-platform">
            Platform
        </div>
        <div class="cart-amount">
            Aantal
        </div>
        <div class="cart-price">
            Prijs
        </div>  
    </div>

    <?php
        foreach ($products as $product) {
    ?>
            <div class="cart-row">
                <div class="cart-product">
                    <?php echo $product['name']." #".$product['id']; ?>
                </div>
                <div class="cart-platform">
                    <?php echo $product['platform']; ?>
                </div>
                <div class="cart-amount">
                    <?php echo $product['amount']; ?>
                </div>
                <div class="cart-price">
                    <?php echo $product['price']*$product['amount']; ?>
                </div>  
            </div>
    <?php
        }
    ?>

    <div class="cart-row">
        <div class="cart-product">
            
        </div>
        <div class="cart-platform">

        </div>
        <div class="cart-amount">
            
        </div>
        <div class="cart-price">
            <?php echo $totalPrice; ?>            
        </div>  
    </div>

</div>
<?php
if (isset($finished)) {
    echo $finished;
} else {
?>
<a href= <?php echo "/admins/finishorder/".$orderId; ?> class="btn-shoppingcart"> Bestelling afronden </a>

<?php
} // !finished
} // !message
?>