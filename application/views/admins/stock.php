<div class="cart">
    <div class="cart-row">
        <div class="cart-platform">
            #
        </div>
        <div class="cart-product">
            Naam            
        </div>
        <div class="cart-amount">
            Verkoopbaar
        </div>
        <div class="cart-user">
            Fysiek
        </div>  
    </div>

<?php
foreach ($products as $product) {
?>
        <div class="cart-row">
            <div class="cart-platform">
                <a href= <?php echo "/users/product/".$product->getId(); ?> ><?php echo $product->getId(); ?></a>
            </div>
            <div class="cart-product">
                <?php echo $product->getName(); ?>
            </div>
            <div class="cart-amount">
                <?php echo $product->getDigitalStock(); ?>
            </div>            
            <div class="cart-user">
                <?php echo $product->getPhysicalStock(); ?>
            </div>  
        </div>
<?php
    }
?>
</div>