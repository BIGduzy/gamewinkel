<div class="cart">
    <div class="cart-row">
        <div class="cart-product">
            #
        </div>
        <div class="cart-platform">
            Status            
        </div>
        <div class="cart-amount">
            Datum
        </div>
        <div class="cart-user">
            Gebruiker
        </div>  
    </div>

<?php
foreach ($orders as $order) {
?>
        <div class="cart-row">
            <div class="cart-product">
                <a href= <?php echo "/admins/order/".$order->getId(); ?> ><?php echo $order->getId(); ?></a>  <?php echo $order->getPreorder() ? " (Preorder)" : ''; ?>
            </div>
            <div class="cart-platform">
                <?php echo $order->getStatus(); ?>
            </div>
            <div class="cart-amount">
                <?php echo $order->getDate(); ?>
            </div>            
            <div class="cart-user">
                <?php echo $order->getUser(); ?>
            </div>  
        </div>
<?php
    }
?>
</div>