<div class="cart">
    <div class="cart-row">
        <div class="cart-product">
            Product
        </div>
        <div class="cart-platform">
            Platform
        </div>
        <div class="cart-amount">
            Aantal
        </div>
        <div class="cart-price">
            Prijs
        </div>  
    </div>

    <?php
    if (isset($message)) {
        echo $message;
    } else {
        foreach ($products as $key => $product) {
    ?>
            <div class="cart-row">
                <div class="cart-product">
                    <?php echo $product['name']." #".$product['id']; ?>
                </div>
                <div class="cart-platform">
                    <?php echo $product['platform']; ?>
                </div>
                <div class="cart-amount">
                    <a href= <?php echo "/customers/decrease/".$key?> class="decrease">-</a> <?php echo $product['amount']; ?> <a href= <?php echo "/customers/increase/".$key ?> class="increase">+</a>
                </div>
                <div class="cart-price">
                    <?php echo $product['price']*$product['amount']; ?>
                </div>  
            </div>
    <?php
        }
    ?>

    <div class="cart-row">
        <div class="cart-product">
            
        </div>
        <div class="cart-platform">

        </div>
        <div class="cart-amount">
            
        </div>
        <div class="cart-price">
            <?php echo $totalPrice; ?>            
        </div>  
    </div>

    <?php
    } // !message
    ?>

</div>

<a href= <?php echo "/customers/orderProduct"; ?> class="btn-shoppingcart"> Afrekenen </a>