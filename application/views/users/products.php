<?php 
    if (isset($message)) {
        echo $message;
    } else {
?>
<select id="platform-filter" onchange="filter(this.value)">
  <option value="0">All</option>
  <option value="1">Xbox one</option>
  <option value="2">Playstation 4</option>
  <option value="3">Pc</option>
  <option value="4">Wii u</option>
</select>
<?php } ?>

<div class="items">
  <?php
  foreach ($products as $product) {
    echo '<div class="item '.$product->getPlatformsIds().'">'; // fix for muli classes problem
  ?>
        <a href= <?php echo "/users/product/".$product->getId().""; ?> >
        <img src= <?php echo $product->getImage() !== null ?  "/games/".$product->getImage() : "/img/gamewinkel.png"; ?> alt=<?php echo $product->getName(); ?> >
        <span> <?php echo $product->getName(); ?> </span>
        <span> (<?php echo $product->getPlatformsString() ?>) </span>
        <div class="price"> <span> <?php echo $product->getPrice(); ?> </span> </div>
        </a>  
    </div>
  <?php
  }
  ?>
</div>