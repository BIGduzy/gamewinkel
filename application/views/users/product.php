<div class="info">
    <img src= <?php echo $product->getImage() !== null ?  "/games/".$product->getImage() : "/img/gamewinkel.png"; ?> >

    <div class="box">
    <form method="POST" action=<?php echo "/customers/addToCart/".$product->getId(); ?>>
        <div class="box-left">
        <span class="title"> <?php echo $product->getName(); ?> </span>
        <span class="platform"> Platform: </span>
        <?php
        foreach ($product->getPlatforms() as $platform) {
        ?>
            <input checked="true" type="radio" name="platform" value="<?php echo $platform->getName(); ?>" id="<?php echo $platform->getName(); ?>"> <label for="<?php echo $platform->getName(); ?>"><?php echo $platform->getName(); ?></label>
        <?php
        }
        ?>
        <input type="number" name="amount" value="1">
        </div>
        
        <div class="box-right">
        <span class="release-date"> <?php echo $product->getReleasedate(); ?> </span>
        <span class="price"> <?php echo $product->getPrice(); ?> </span>
        </div>
        
      </div>
      
      <input type="submit" class="btn-shoppingcart" value="In winkelmandje">
    
    </form>
</div>

<div class="description">
    <span class="title"> <?php echo $product->getName(); ?> </span>
    <p> <?php echo $product->getDescription(); ?> </p>
    <?php if ($product->getYoutube() !== null) {?>
    <div class="youtube">
        <iframe width="560" height="349" src= <?php echo "https://www.youtube.com/embed/".$product->getYoutube(); ?> frameborder="0" allowfullscreen></iframe>
    </div>
    <?php } ?>
    <?php if ($product->getSound() !== null) { ?>
    <audio controls>
      <source src= <?php echo "/games/".$product->getSound(); ?> type="audio/mpeg">
      Your browser does not support the audio tag.
    </audio>
    <?php } ?>

    <?php if ($product->getScreenshot() !== null) { ?>
    <div class="screenshot">
      <img src= <?php echo "/games/".$product->getScreenshot(); ?> alt="Screenshot">
    </div>
    <?php } ?>
</div>