<?php
    echo '<a href="/users/products" class="left">Games</a>';
  
  if (!isset($_SESSION['userRole'])) {  // not logged in
    echo '<a href="/users/login" class="right">Login</a>';
    echo '<a href="/users/register" class="right">Registreer</a>';
  } else {  
    if ($_SESSION['userRole'] == 0) { // customer
      echo '<a href="/customers/logout" class="right">Logout</a>';
      echo '<a href="/customers/shoppingcart" class="right">shoppingcart</a>';
      echo '<a href="/customers/orders" class="right">Bestellingen</a>';
    }
    
    if ($_SESSION['userRole'] == 1) { // admin
      echo '<a href="/customers/logout" class="right">Logout</a>';
      echo '<a href="/admins/barcode" class="right">Barcode</a>';
      echo '<a href="/admins/orders" class="right">Bestellingen</a>';
      echo '<a href="/admins/supplys" class="right">Inkopen</a>';
      echo '<a href="/admins/stock" class="left">Voorrrad</a>';
    }

    if ($_SESSION['userRole'] == 2) { // admin
      echo '<a href="/customers/logout" class="right">Logout</a>';
      echo '<a href="/admins/barcode" class="right">Barcode</a>';
    }
  }

?>