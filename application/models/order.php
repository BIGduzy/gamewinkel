<?php
/**
*   Order class is uses for orders
*
*/
class Order {

    private $id;
    private $user;
    private $products;
    private $status;
    private $date;
    private $preorder;

    /**
    * Default constructor
    *
    */ 
    public function __construct() {
    }

    /**
    *   Getter for order id
    *
    *   @return int id
    *
    */
    public function getId() {
        return $this->id;
    }

    /**
    * Setter for order id
    *
    * @param int $id New value for id
    * @return mixed
    *
    */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
    *   Getter for user
    *
    *   @return int userId
    *
    */
    public function getUser() {
        return $this->user;
    }

    /**
    * Setter for user
    *
    * @param int $user New userId
    * @return mixed
    *
    */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
    *   Getter for order products
    *
    *   @return array products
    *
    */
    public function getProducts() {
        return $this->products;
    }

    /**
    * Setter for order products
    *
    * @param array $products New array with products
    * @return mixed
    *
    */
    public function setProducts($products) {
        $this->products = $products;
        return $this;
    }

    /**
    *   Getter for this order status
    *
    *   @return int statusId
    *
    */
    public function getStatus() {
        return $this->status;
    }

    /**
    * Setter for order status
    *
    * @param int $status New value for statusId
    * @return mixed
    *
    */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
    *   Getter for this order date
    *
    *   @return date date
    *
    */
    public function getDate() {
        return $this->date;
    }

    /**
    * Setter for order id
    *
    * @param date $date New value for date
    * @return mixed
    *
    */
    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

    /**
    *   Getter for preorder
    *
    *   @return boolean Is pre order true or false
    *
    */
    public function getPreorder() {
        return $this->preorder;
    }

    /**
    * Setter for order id
    *
    * @param boolean $preorder Is pre order true or false
    * @return boolean
    *
    */
    public function setPreorder($preorder) {
        $this->preorder = $preorder;
        return $this;
    }

}