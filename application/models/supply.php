<?php
/**
*   Supply class is uses for orders
*
*/
class Supply {

    private $id;
    private $user;
    private $products;
    private $status;
    private $date;

    /**
    * Default constructor
    *
    */ 
    public function __construct() {
    }

    /**
    *   Getter for supply id
    *
    *   @return int id
    *
    */
    public function getId() {
        return $this->id;
    }

    /**
    * Setter for supply id
    *
    * @param int $id New value for id
    * @return mixed
    *
    */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
    *   Getter for user
    *
    *   @return int userId
    *
    */
    public function getUser() {
        return $this->user;
    }

    /**
    * Setter for user
    *
    * @param int $user New userId
    * @return mixed
    *
    */
    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    /**
    *   Getter for supply products
    *
    *   @return array products
    *
    */
    public function getProducts() {
        return $this->products;
    }

    /**
    * Setter for supply products
    *
    * @param array $products New array with products
    * @return mixed
    *
    */
    public function setProducts($products) {
        $this->products = $products;
        return $this;
    }

    /**
    *   Getter for this supply status
    *
    *   @return int statusId
    *
    */
    public function getStatus() {
        return $this->status;
    }

    /**
    * Setter for supply status
    *
    * @param int $status New value for statusId
    * @return mixed
    *
    */
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    /**
    *   Getter for this supply date
    *
    *   @return date date
    *
    */
    public function getDate() {
        return $this->date;
    }

    /**
    * Setter for supply id
    *
    * @param date $date New value for date
    * @return mixed
    *
    */
    public function setDate($date) {
        $this->date = $date;
        return $this;
    }

}