<?php
class platform {
	private $id;
	private $name;

	public function __construct($platform) {
		$this->id = $platform['id'];
		$this->name = $platform['name'];
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}
}
?>