<?php
class Admin extends model {

    /**
    *   Gets user name from the database
    *   @param $userId  The id of the user
    *   @return string User name
    *
    */
    public function getUsernameById($userId) {
        $query = "SELECT `name` FROM `user` WHERE `id` = '$userId' LIMIT 1;";
        return $this->query($query)[0]['User']['name'];
    }

	/**
	*	Gets order by id from the database
	*	@param $orderId the id of the order
	*	@return array Array with the order data
	*
	*/
	public function getOrderByOrderId($orderId) {
        $query = "SELECT `order`.*,
                         `product`.*,
                         `platform`.name
                  FROM `order_product_junction` AS `oj`
                  JOIN `order` AS `order` ON `order`.id = `oj`.order_id
                  JOIN `product` AS product ON `product`.id = `oj`.product_id
                  JOIN `platform` AS platform ON `platform`.id = `oj`.platform_id
                  WHERE `oj`.order_id = {$orderId}";
        return $this->query($query);
    }

    /**
    *	Updates the order status in database
    *	@param $orderId id of the order
    *	@param $status The new status of the order
    *
    */
    public function updateOrderStatus($orderId, $status) {
    	$query = "UPDATE `order` SET `status` = {$status} WHERE `id` = {$orderId};";
    	$this->query($query);
    }


    /**
    *   Updates the physical stock of given product in database
    *   @param int $productId the id of the product that needs to be updated
    *   @param int $amount the new amount of stock
    *
    */
    public function updatePhysicalStock($product, $amount) {
        $query = "UPDATE `physical_stock` SET `amount` = {$amount} WHERE `product` = {$product};";
        $this->query($query);
    }

    /**
    *   Gets physical stock of product from database
    *   @param int $id The id of the product
    *   @return int The amount of products left
    *
    */
    public function getPhysicalStockByProductId($id) {
        $query = "SELECT * FROM `physical_stock` WHERE `physical_stock`.`product` = {$id};";
        return $this->query($query)[0]['Physical_stock']['amount'];
    }

    /**
    *   Gets digital stock of product
    *   @param int $id The id of the product
    *   @return int The amount of products left
    *
    */
    public function getDigitalStockByProductId($id) {
        $query = "SELECT * FROM `digital_stock` WHERE `digital_stock`.`product` = {$id};";
        return $this->query($query)[0]['Digital_stock']['amount'];
    }

    /**
    *   Gets all orders from the database
    *   @return array Array with the order data
    *
    */
    public function getOrders() {
        $query = "SELECT * FROM `order`;";
        return $this->query($query);
    }

    /**
    *   Gets all supplys from the database
    *   @return array Array with the supply data
    *
    */
    public function getSupplys() {
        $query = "SELECT * FROM `supply`;";
        return $this->query($query);
    }

    /**
    *   Gets supply by id from the database
    *   @param $supplyId the id of the supply
    *   @return array Array with the supply data
    *
    */
    public function getSupplyBySupplyId($supplyId) {
        $query = "SELECT `supply`.*,
                         `product`.*,
                         `platform`.name
                  FROM `supply_product_junction` AS `oj`
                  JOIN `supply` AS `supply` ON `supply`.id = `oj`.supply_id
                  JOIN `product` AS product ON `product`.id = `oj`.product_id
                  JOIN `platform` AS platform ON `platform`.id = `oj`.platform_id
                  WHERE `oj`.supply_id = {$supplyId}";
        return $this->query($query);
    }

    /**
    *   Gets products from database
    *   @return array Array with products data
    *
    */
    public function getProducts() {
        $query = "SELECT DISTINCT product.*, group_concat(pl.id) as Platforms FROM platform_product_junction as pj JOIN platform as pl ON pl.id = pj.platform_id JOIN product as product ON product.id = pj.product_id GROUP BY product.id;";
        return $this->query($query);
    }


    /**
    *   Gets platform with the given id from the database
    *   @param int $id The id of needed platfrom
    *   @return array Array with name and id of the platfrom
    *
    */
    public function getPlatformById($id) {
        $query = "SELECT * FROM platform WHERE id ='".$id."' LIMIT 1";
        return $this->query($query)[0]['Platform'];
    }
}