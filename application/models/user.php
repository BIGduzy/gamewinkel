<?php
    class User extends Model {

        public function checkUser($email,$password) {
            $query = "SELECT id,role FROM `user` WHERE email = '".$email."' AND password = '".$password."' LIMIT 1;";
            return $this->query($query);
        }

        public function checkEmail($email) {
            $query = "SELECT id,role FROM `user` WHERE email = '".$email."' LIMIT 1;";
            return $this->query($query);
        }

        /**
        *   Inserts given user into the database
        *   @param User $user The user that needs to be inserted
        *   @return int The id of the inserded user
        *
        */
        public function insertUser($user) {
            $name = $user['firstname'] . ' ' . $user['prefix'] . ' ' . $user['lastname'];            $address = $user['street'] . ' ' . $user['zipcode'] . ' ' . $user['city'];
            $query = "INSERT INTO `user` (email, password, name, address, phone, role) 
                      VALUES ('{$user['email']}',
                              '{$user['password']}',
                              '{$name}',
                              '{$address}',
                              '{$user['phone']}',
                              0);";
            $this->query($query);
            
            $userId = $this->find_last_inserted_id();
            return $userId;
        }

        public function getProducts() {
            $query = "SELECT DISTINCT product.*, group_concat(pl.id) as Platforms FROM platform_product_junction as pj JOIN platform as pl ON pl.id = pj.platform_id JOIN product as product ON product.id = pj.product_id GROUP BY product.id;";
            return $this->query($query);
        }

        /**
        *   Searched the database for all products with given title
        *   @param $title The title of the search
        *   @return array Array with the products
        *
        */
        public function getProductsByTitle($title) {
            $query = "SELECT DISTINCT product.*,
                                      group_concat(pl.id) as Platforms
                      FROM platform_product_junction as pj
                      JOIN platform as pl ON pl.id = pj.platform_id
                      JOIN product as product ON product.id = pj.product_id
                      WHERE `product`.name LIKE '%{$title}%'
                      GROUP BY product.id;";
            return $this->query($query);
        }

        /**
        *   Gets platform with the given id from the database
        *   @param int $id The id of needed platfrom
        *   @return array Array with name and id of the platfrom
        *
        */
        public function getPlatformById($id) {
            $query = "SELECT * FROM platform WHERE id ='".$id."' LIMIT 1";
            return $this->query($query)[0]['Platform'];
        }

        public function getProductsLimit($limit) {
            $query = "SELECT DISTINCT product.*, group_concat(pl.id) as Platforms FROM platform_product_junction as pj JOIN platform as pl ON pl.id = pj.platform_id JOIN product as product ON product.id = pj.product_id GROUP BY product.id LIMIT ".$limit.";";
            return $this->query($query);
        }

        public function getProductById($id) {
            $query = "SELECT DISTINCT product.*, group_concat(pl.id) as Platforms FROM platform_product_junction as pj JOIN platform as pl ON pl.id = pj.platform_id JOIN product as product ON product.id = pj.product_id WHERE product.id = '".$id."' GROUP BY product.id LIMIT 1;";
            return $this->query($query)[0];
        }
    }
?>