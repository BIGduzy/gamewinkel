<?php
class Product {
    private $id;
    private $name;
    private $description;
    private $price;
    private $releasedate;
    private $image;
    private $screenshot;
    private $youtube;
    private $sound;
    private $platforms;
    private $physicalStock;
    private $digitalStock;

    public function __construct($product) {
        $this->id = $product['Product']['id'];
        $this->name = $product['Product']['name'];
        $this->description = $product['Product']['description'];
        $this->price = $product['Product']['price'];
        $this->releasedate = $product['Product']['releasedate'];
        $this->image = $product['Product']['image'];
        $this->screenshot = $product['Product']['screenshot'];
        $this->youtube = $product['Product']['youtube'];
        $this->sound = $product['Product']['sound'];
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function getReleasedate() {
        return $this->releasedate;
    }

    public function setReleasedate($releasedate) {
        $this->releasedate = $releasedate;
        return $this;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

    public function getScreenshot() {
        return $this->screenshot;
    }

    public function setScreenshot($screenshot) {
        $this->screenshot = $screenshot;
        return $this;
    }

    public function getYoutube() {
        return $this->youtube;
    }

    public function setYoutube($youtube) {
        $this->youtube = $youtube;
        return $this;
    }

    public function getSound() {
        return $this->sound;
    }

    public function setSound($sound) {
        $this->sound = $sound;
        return $this;
    }

    public function getPlatforms() {
        return $this->platforms;
    }

    public function setPlatforms($platforms) {
        $this->platforms = $platforms;
        return $this;
    }

    /**
    *   Returns the names of all the platforms in sting format
    *
    *   @return string The sting of the platforms divided by comma's
    */
    public function getPlatformsString() {
        $names = [];
        foreach ($this->platforms as $platform) {
            array_push($names, $platform->getName());
        }
        return implode(", ", $names);
    }

    /**
    *   Returns the id of all the platforms in sting format
    *
    *   @return string The sting of the platforms divided by spaces
    */
    public function getPlatformsIds() {
        $names = [];
        foreach ($this->platforms as $platform) {
            array_push($names, $platform->getId());
        }
        return implode(" ", $names);
    }

    public function getPhysicalStock() {
        return $this->physicalStock;
    }

    public function setPhysicalStock($physicalStock) {
        $this->physicalStock = $physicalStock;
        return $this;
    }

    public function getDigitalStock() {
        return $this->digitalStock;
    }

    public function setDigitalStock($digitalStock) {
        $this->digitalStock = $digitalStock;
        return $this;
    }
}

