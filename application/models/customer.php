<?php
class Customer extends Model {
  
    // public function getProducts() {
    //     $query = "SELECT * FROM product;";
    //     return $this->query($query);
    // }

    public function getProductById($id) {
        $query = "SELECT DISTINCT product.*, group_concat(pl.name) as Platforms FROM platform_product_junction as pj JOIN platform as pl ON pl.id = pj.platform_id JOIN product as product ON product.id = pj.product_id WHERE product.id = '".$id."' GROUP BY product.id LIMIT 1;";
        return $this->query($query);
    }

    /**
    *   Gets username by user id from the database
    *   @param in $id The id of the user
    *   @return string name of the user
    *
    */
    public function getUsernameById($id) {
        $query = "SELECT `name` FROM `user` WHERE `id` = {$id} LIMIT 1;";
        return $this->query($query)[0]['User']['name'];
    }

    /**
    *   Gets user by user id from the database
    *   @param in $id The id of the user
    *   @return array Array with the user data
    *
    */
    public function getUserById($id) {
        $query = "SELECT * FROM `user` WHERE `id` = {$id} LIMIT 1;";
        return $this->query($query)[0]['User'];
    }

    /**
    *   Gets digital stock of product
    *   @param int $id The id of the product
    *   @return int The amount of products left
    *
    */
    public function getDigitalStockByProductId($id) {
        $query = "SELECT * FROM `digital_stock` WHERE `digital_stock`.`product` = {$id};";
        return $this->query($query)[0]['Digital_stock']['amount'];
    }

    /**
    *   Gets platform with the given id from the database
    *   @param int $id The id of needed platfrom
    *   @return array Array with name and id of the platfrom
    *
    */
    public function getPlatformById($id) {
        $query = "SELECT * FROM platform WHERE id ='".$id."' LIMIT 1";
        return $this->query($query)[0]['Platform'];
    }

    /**
    *   Inserts given order into the database
    *   @param Order $order order object that needs to be inserted
    *   @param boolean $preorder Is preorder true false
    *   @return int the id of the inserted order
    *
    */
    public function insertOrder($order, $preorder) {
        $preorder = $preorder ? 1 : 0;
        $query = "INSERT INTO `order` (id, user, status, order_date, preorder) VALUES (null, {$order->getUser()}, 0, '{$order->getDate()}', {$preorder});";
        $this->query($query);

        // set last inset id
        $orderId = $this->find_last_inserted_id();

        foreach ($order->getProducts() as $product ) {
            $query = "INSERT INTO `order_product_junction` (order_id, product_id, platform_id) VALUES ({$orderId}, {$product->getId()}, {$product->getPlatforms()->getId()});";
            $this->query($query);
        }

        return $orderId;
    }

    /**
    *   Updates the digital stock of given product
    *   @param int $productId the id of the product that needs to be updated
    *   @param int $amount the new amount of stock
    *
    */
    public function updateDigitalStock($product, $amount) {
        $query = "UPDATE `digital_stock` SET `amount` = {$amount} WHERE `product` = {$product};";
        $this->query($query);
    }

    public function getOrdersByUser($userId) {
        $query = "SELECT * FROM `order` WHERE user = {$userId}";
        return $this->query($query);
    }

    public function getOrderByOrderId($orderId) {
        $query = "SELECT `order`.*,
                         `product`.*,
                         `platform`.name
                  FROM `order_product_junction` AS `oj`
                  JOIN `order` AS `order` ON `order`.id = `oj`.order_id
                  JOIN `product` AS product ON `product`.id = `oj`.product_id
                  JOIN `platform` AS platform ON `platform`.id = `oj`.platform_id
                  WHERE `oj`.order_id = {$orderId}";
        return $this->query($query);
    }
}