<?php
class AdminsController extends controller {

    public function order($orderId) {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1 || $_SESSION['userRole'] !== 2) { // Not admin or salesclerk
                header('Location:/users/homepage');
                exit;
            }
        }

        $order = $this->_model->getOrderByOrderId($orderId);

        if (empty($order)) {
            $this->set('message', 'Order: #' . $orderId . ' niet bekent.');
            exit;
        }

        if (intval($order[0]['Order']['status']) === 1) {
            // $this->set('message', 'Order: #' . $orderId . ' is al afgerond.');
            // exit;
            $this->set('finished', 'Deze bestelling is al afgerond');
        }

        // Group by id and add amount
        $totalPrice = 0;
        $shoppingcart = [];
        foreach ($order as $data) {
            
            $id = $data['Product']['id'];
            if (isset($shoppingcart[$id])) {
                $shoppingcart[$id]['amount']++;
            } else {
                $shoppingcart[$id] = [
                                       'id' => $data['Product']['id'],
                                       'name' => $data['Product']['name'],
                                       'platform' => $data['Platform']['name'],
                                       'amount' => 1,
                                       'price' => $data['Product']['price']
                                     ];
            }
            
            $totalPrice += $shoppingcart[$id]['price'];
        }

        $total = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
        $user = $this->_model->getUsernameById($order[0]['Order']['user']);
        $this->set('user',$user);
        $this->set('date', $order[0]['Order']['order_date']);
        $this->set('status',$order[0]['Order']['status']);
        $this->set('orderId', $orderId);
        $this->set('total', $total);
        $this->set('totalPrice', $totalPrice);
        $this->set('products', $shoppingcart);
    }

    public function finishOrder($orderId) {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1 || $_SESSION['userRole'] !== 2) { // Not admin or salesclerk
                header('Location:/users/homepage');
                exit;
            }
        }

        // TODO: physical stock
        $rawOrder = $this->_model->getOrderByOrderId($orderId);

        $products;
        // Group products
        foreach ($rawOrder as $data) {
            
            $id = $data['Product']['id'];
            if (isset($products[$id])) {
                $products[$id]['amount']++;
            } else {
                $products[$id] = [
                                   'id' => $data['Product']['id'],
                                   'amount' => 1
                                 ];
            }
        }

        // update physical stock
        foreach ($products as $product) {
            $stock = $this->_model->getPhysicalStockByProductId($product['id']);
            $newStock = $stock - $product['amount'];
            $this->_model->updatePhysicalStock($product['id'], $newStock);
            
        }
        $this->_model->UpdateOrderStatus($orderId,1);

        $this->set('message', 'Bestelling afgerond');
    }

    public function barcode() {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1 || $_SESSION['userRole'] !== 2) { // Not admin or salesclerk
                header('Location:/users/homepage');
                exit;
            }
        }

        if (isset($_POST['barcode'])) {
            header('Location:/admins/order/'.$_POST['barcode']);
        }
    }

    public function orders() {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1) { // Not admin
                header('Location:/users/homepage');
                exit;
            }
        }

        $orders = $this->_model->getOrders();

        $array = [];
        foreach ($orders as $o ) {
           $order = new Order();
           $order->setId($o['Order']['id']);
           $order->setUser($o['Order']['user']);
           $order->setStatus($o['Order']['status']);
           $order->setDate($o['Order']['order_date']);
           $order->setPreorder($o['Order']['preorder']);

           array_push($array, $order);
        }

        $this->set('orders',$array);
    }

    public function supplys() {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1) { // Not admin
                header('Location:/users/homepage');
                exit;
            }
        }

        $supplys = $this->_model->getSupplys();

        $array = [];
        foreach ($supplys as $value ) {
           $order = new Supply();
           $order->setId($value['Supply']['id']);
           $order->setUser($value['Supply']['user']);
           $order->setStatus($value['Supply']['status']);
           $order->setDate($value['Supply']['date']);

           array_push($array, $order);
        }

        $this->set('supplys',$array);
    }

    public function supply($supplyId) {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1 || $_SESSION['userRole'] !== 2) { // Not admin or salesclerk
                header('Location:/users/homepage');
                exit;
            }
        }

        $supply = $this->_model->getSupplyBySupplyId($supplyId);

        if (empty($supply)) {
            $this->set('message', 'Inkoop: #' . $supplyId . ' niet bekent.');
            exit;
        }

        // if (intval($supply[0]['Supply']['status']) === 1) {
        //     $this->set('message', 'Inkoop: #' . $supplyId . ' is al afgerond.');
        //     exit;
        // }

        // Group by id and add amount
        $totalPrice = 0;
        $shoppingcart = [];
        foreach ($supply as $data) {
            
            $id = $data['Product']['id'];
            if (isset($shoppingcart[$id])) {
                $shoppingcart[$id]['amount']++;
            } else {
                $shoppingcart[$id] = [
                                       'id' => $data['Product']['id'],
                                       'name' => $data['Product']['name'],
                                       'platform' => $data['Platform']['name'],
                                       'amount' => 1,
                                       'price' => $data['Product']['price']
                                     ];
            }
            
            $totalPrice += $shoppingcart[$id]['price'];
        }

        $total = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
        $user = $this->_model->getUsernameById($supply[0]['Supply']['user']);
        $this->set('user',$user);
        $this->set('date', $supply[0]['Supply']['date']);
        $this->set('status',$supply[0]['Supply']['status']);
        $this->set('supplyId', $supplyId);
        $this->set('total', $total);
        $this->set('totalPrice', $totalPrice);
        $this->set('products', $shoppingcart);
    }

    public function stock() {
        if (!isset($_SESSION['userID']) || !isset($_SESSION['userRole'])) {
            if ($_SESSION['userRole'] !== 1) { // Not admin or salesclerk
                header('Location:/users/homepage');
                exit;
            }
        }

        $rawProducts = $this->_model->getProducts();

        $products = [];
        foreach ($rawProducts as $value) {
            $platformIds = explode(',', $value['']['Platforms']);
            $platforms = [];
            foreach ($platformIds as $platformId) {                
                $platform = new Platform($this->_model->getPlatformById($platformId));
                array_push($platforms, $platform);
            }
            $product = new Product($value);
            $product->setPlatforms($platforms);

            $physicalStock = $this->_model->getPhysicalStockByProductId($product->getId());
            $product->setPhysicalStock($physicalStock);

            $digitalStock = $this->_model->getDigitalStockByProductId($product->getId());
            $product->setDigitalStock($digitalStock);
            array_push($products, $product);
        }
        $this->set('products',$products);
    }
}