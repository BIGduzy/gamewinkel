<?php
class CustomersController extends Controller {
    public function homepage() {

    }

    public function logout() {    
        session_destroy();
        header('Location:/users/homepage');
    }

    public function increase($itemId) {
        $_SESSION['cart'][$itemId]['amount'] ++;
        header('Location:/customers/shoppingcart');
    }

    public function decrease($itemId) {
        $_SESSION['cart'][$itemId]['amount'] --;

        if ($_SESSION['cart'][$itemId]['amount'] < 1) {
            unset($_SESSION['cart'][$itemId]);
        }
        header('Location:/customers/shoppingcart');
    }

    public function addToCart($productId) {
        if ($_POST['amount'] < 1) {
            header('Location:/users/homepage');
            exit;
        }

        $shoppingCart = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];
        $platformId = 0;

        switch ($_POST['platform']) {
            case "Wii u":
                $platformId = 4;
            break;
            case "Xbox one":
                $platformId = 1;
            break;
            case "Pc":
                $platformId = 3;
            break;
            case "Playstation 4":
                $platformId = 2;
            break;

            default:
                $platformId = id;
            break;
        }

        $new = false;
        foreach ($shoppingCart as $key => $value) {
            if ($productId === $value['productId'] && $platformId === $value['platformId']) {
                $shoppingCart[$key]['amount'] += $_POST['amount'];
            } else {
                $new = true;
            }
        }

        if (empty($shoppingCart) || $new) {
            array_push($shoppingCart,['productId' => $productId, 'platformId' => $platformId, 'amount' => intval($_POST['amount'])]);
        }

        $_SESSION['cart'] = $shoppingCart;
        header('Location:/customers/shoppingcart');
    }

    public function orderProduct() {
        // No shopping cart
        if (!isset($_SESSION['cart'])) {
            header('Location:/users/homepage');
            exit;
        }

        // Not loggd in
        if (!isset($_SESSION['userID'])) {
            header('Location:/users/login');
            exit;
        }

        // Create products
        $products = [];
        $preorderProducts = [];
        foreach ($_SESSION['cart'] as $cart) {
            $platform = new Platform($this->_model->getPlatformById($cart['platformId']));
            $product = new Product($this->_model->getProductById($cart['productId'])[0]);
            $product->setPlatforms($platform);

            $releaseDate = new DateTime($product->getReleasedate());
            $now = new DateTime(date('Y-m-d'));

            // Pre order
            if ($releaseDate > $now) {
                for ($i = 0; $i < $cart['amount']; $i++) {
                    array_push($preorderProducts, $product);
                }
            } else {
                for ($i = 0; $i < $cart['amount']; $i++) {
                    array_push($products, $product);
                }
            }            
        }

        // Create order
        $orderController = new OrderController();
        $order = $orderController->createOrder($_SESSION['userID'], $products);

        // Create new order for the pre oder products
        if (!empty($preorderProducts)) {
            $preorder = $orderController->createOrder($_SESSION['userID'], $preorderProducts);
        }

        // Create array to diplay shopping cart
        $result = [];
        $totalPrice = 0;
        $preorderResult = [];
        $preorderTotalPrice = 0;
        foreach ($_SESSION['cart'] as $key => $cart) {

            $platform = new Platform($this->_model->getPlatformById($cart['platformId']));
            $product = new Product($this->_model->getProductById($cart['productId'])[0]);
            $product->setPlatforms($platform);

            $releaseDate = new DateTime($product->getReleasedate());
            $now = new DateTime(date('Y-m-d'));

            // Pre order
            if ($releaseDate > $now) {
                $preorderResult[$key] = ['id' => $product->getId(),
                                   'name' => $product->getName(),
                                   'platform' => $product->getPlatforms()->getName(),
                                   'amount' => $cart['amount'],
                                   'price' => $product->getPrice()];

                $preorderTotalPrice += ($product->getPrice() * $cart['amount']."as");
            } else {
                $result[$key] = ['id' => $product->getId(),
                                   'name' => $product->getName(),
                                   'platform' => $product->getPlatforms()->getName(),
                                   'amount' => $cart['amount'],
                                   'price' => $product->getPrice()];
                $totalPrice += $cart['amount'] * $product->getPrice();
            } 

            $stock = $this->_model->getDigitalStockByProductId($product->getId());
            if ($stock - $cart['amount'] < 0) {
                $this->set('message','Helaas is het product: '.$product->getName()." Niet meer op voorraad");
                exit;
            } else {
                // Update digital stock
                $this->_model->UpdateDigitalStock($product->getId(), $stock - $cart['amount']);
            }
        }

        // Insert order in database (returns order id)
        $orderId = $this->_model->insertOrder($order, false);

        // Insert pre order products in the database;
        if (!empty($preorder)) {
            $preorderId = $this->_model->insertOrder($preorder, true);
        }

        // Get raw user data
        $user = $this->_model->getUserById($_SESSION['userID']);

        // ********************************
        // ****** Pdf http://fpdf.org *****
        // ** ~~V/en/script/script85.php **
        // ********************************
        $dir = $this->createOrderPdf($orderId, $order->getDate(), $result, $user['name'], $totalPrice);

        // Pre order pdf
        if (!empty($preorderResult)) {
            $preorderDir = $this->createOrderPdf($preorderId, $preorder->getDate(), $preorderResult, $user['name'], $preorderTotalPrice);
        }

        // *************************
        // ********  Email  ********
        // *************************
        $succes = $this->sendOrderMail($user, $orderId, $dir);

        if (isset($preorderDir)) {
            $preorderSucces = $this->sendOrderMail($user, $preorderId, $preorderDir);
        }

        // Send the email
        if(!$succes) {
            // Set message for view
            $this->set('message','Oeps! Er is iets mis gegaan, probeer het later opnieuw.');
            exit();
        } else {
            // Empty shoppincart
            unset($_SESSION['cart']);

            // Set message for view
            $this->set('message','Bedankt voor uw bestelling!');
        }
    }

    public function orders() {
        if (!isset($_SESSION['userID'])) {
            header('Location:/users/homepage');
            exit;
        }

        $orders = $this->_model->getOrdersByUser($_SESSION['userID']);

        $array = [];
        foreach ($orders as $o ) {
           $order = new Order();
           $order->setId($o['Order']['id']);
           $order->setUser($o['Order']['user']);
           $order->setStatus($o['Order']['status']);
           $order->setDate($o['Order']['order_date']);
           $order->setPreorder($o['Order']['preorder']);

           array_push($array, $order);
        }

        $this->set('orders',$array);
    }

    public function order($orderId) {
        if (!isset($_SESSION['userID'])) {
            header('Location:/users/homepage');
            exit;
        }

        $products = $this->_model->getOrderByOrderId($orderId);
        // Not correct user
        if ($products[0]['Order']['user'] !== $_SESSION['userID']) {
            header('Location:/users/homepage');
            exit;
        }

        // Group by id and add amount
        $totalPrice = 0;
        $shoppingcart = [];
        foreach ($products as $data) {
            
            $id = $data['Product']['id'];
            if (isset($shoppingcart[$id])) {
                $shoppingcart[$id]['amount']++;
            } else {
                $shoppingcart[$id] = [
                                       'id' => $data['Product']['id'],
                                       'name' => $data['Product']['name'],
                                       'platform' => $data['Platform']['name'],
                                       'amount' => 1,
                                       'price' => $data['Product']['price']
                                     ];
            }
            
            $totalPrice += $shoppingcart[$id]['price'];
        }

        $total = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
        $this->set('date', $products[0]['Order']['order_date']);
        $this->set('status',$products[0]['Order']['status']);
        $this->set('orderId', $orderId);
        $this->set('total', $total);
        $this->set('totalPrice', $totalPrice);
        $this->set('products', $shoppingcart);
    }

    public function shoppingCart() {
        if (empty($_SESSION['cart'])) {
            $this->set('message', 'Winkelwagen is leeg');
            exit;
        }

        // create array to diplay shopping cart
        $totalPrice = 0;        
        $shoppingcart = [];
        foreach ($_SESSION['cart'] as $key => $cart) {

            $platform = new Platform($this->_model->getPlatformById($cart['platformId']));
            $product = new Product($this->_model->getProductById($cart['productId'])[0]);
            $product->setPlatforms($platform);

            $shoppingcart[$key] = ['id' => $product->getId(),
                               'name' => $product->getName(),
                               'platform' => $product->getPlatforms()->getName(),
                               'amount' => $cart['amount'],
                               'price' => $product->getPrice()];

            $totalPrice += ($product->getPrice() * $cart['amount']);
        }

        $total = isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
        $this->set('total',$total);
        $this->set('totalPrice', $totalPrice);
        $this->set('products',$shoppingcart);
    }

    /**
    *   Creates a pdf for the orders
    *   @param $orderId the id of the order
    *   @param $orderDate The date when order was created
    *   @param $cartArray The array with products
    *   @param $username The name of the user
    *   @return name of the pfd
    *
    */
    public function createOrderPdf($orderId, $orderDate, $cartArray, $username, $totalPrice) {
        require_once(FPDF_LIB);
        $pdf = new PDF_Codabar();

        $pdf->AddPage();
        $pdf->SetFont("Arial", "B", 14);
        $pdf->Cell(40,10, "Bedankt voor uw bestelling[#".$orderId."] {$username}: ");
        $pdf->SetFont("Arial", "", 12);
        $pdf->Ln();
        $pdf->Cell(40,10, "Bestellings datum: ".$orderDate);
        $pdf->Ln();
        $pdf->Cell(40,10, "Dit document dient meegenomen te worden bij het ophalen van uw bestelling!");
        $pdf->Ln();
        $pdf->Ln();        
        $pdf->Cell(10,10, '#',1);
        $pdf->Cell(70,10, 'Naam',1);
        $pdf->Cell(40,10, 'Platform',1);
        $pdf->Cell(20,10, 'Prijs',1);
        $pdf->Cell(20,10, 'aantal',1);
        $pdf->Cell(30,10, 'Totaal Prijs',1);
        $pdf->Ln();

        // For each product create row
        foreach ($cartArray as $value) {

            $pdf->Cell(10,10, $value['id'],1);
            $pdf->Cell(70,10, $value['name'],1);
            $pdf->Cell(40,10, $value['platform'],1);
            $pdf->Cell(20,10, isset($value['discountPrice']) ? $value['price'] . " (" .$value['discountPrice'] . ")" : $value['price'],1);
            $pdf->Cell(20,10, $value['amount'],1);
            $pdf->Cell(30,10, isset($value['discountPrice']) ? $value['discountPrice']*$value['amount'] : $value['price']*$value['amount'],1);

            $pdf->Ln();
        }

        $pdf->Ln();
        $pdf->Cell(30,10,"Totaal prijs: " . $totalPrice);
        $pdf->Codabar(150,275,$orderId);

        // Save document on server
        $dir = $_SERVER['DOCUMENT_ROOT']."/public/pdf/".$orderId.".pdf";
        $pdfdoc = $pdf->Output($dir, "F");

        return $dir;
    }

    /**
    *   Send mail for order
    *   @param $user Array with email and name
    *   @param $orderId The Id of the order
    *   @param $dir The directory of the order pdf
    *   @return boolean Send mail succes true or false
    *
    */
    public function sendOrderMail($user, $orderId, $dir) {
        require_once (PHPMAILER_LIB);
        $mail = new PHPMailer();

        // Google SMTP
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = "smtp.gmail.com"; // sets GMAIL as the SMTP server
        $mail->SMTPAuth = true; // enable SMTP authentication
        $mail->Port = 465; // set the SMTP port for the GMAIL server
        $mail->Username = "samuraaaaaaai@gmail.com"; // GMAIL username
        $mail->Password = "samura*7i"; // GMAIL password
        $mail->SMTPSecure = "ssl"; // sets the prefix to the servier

        $mail->setFrom('GameWinkel@info.nl', 'GameWinkel');   // From
        $mail->addAddress($user['email'], $user['name']);     // Add a recipient
        $mail->addAttachment($dir, 'Bestelling.pdf');           // Add pdf as attachment
        $mail->isHTML(true);                               // Set email format to HTML

        $mail->Subject = 'GameWinkel Bestelling #'.$orderId;          // Add subject
        $mail->Body    = 'Bedankt voor uw bestelling bij GameWinkel. <br> Zie de bijlage voor extra informatie over uw bestelling';
        $mail->AltBody = 'Bedankt voor uw bestelling bij GameWinkel. Zie de bijlage voor extra informatie over uw bestelling'; // No html body

        return $mail->send();
    }
}
