<?php
class OrderController {

    private $orders;

    public function __construct() {
        $this->orders = [];
    }

    public function createOrder($user_id, $products, $status = null) {
        $order = new Order();
        $order->setUser($user_id);
        $order->setProducts($products);

        if (isset($status)) {
            $order->setStatus($status);
        }
        $order->setDate(date('Y-m-d'));

        array_push($this->orders, $order);
        return $order;
    }
}
?>