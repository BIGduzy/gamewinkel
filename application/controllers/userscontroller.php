<?php
class UsersController extends controller {
    public function homepage() {
        $rawProducts = $this->_model->getProductsLimit(8);
        $products = [];
        foreach ($rawProducts as $value) {
            $platformIds = explode(',', $value['']['Platforms']);
            $platforms = [];
            foreach ($platformIds as $platformId) {                
                $platform = new Platform($this->_model->getPlatformById($platformId));
                array_push($platforms, $platform);
            }
            $product = new Product($value);
            $product->setPlatforms($platforms);
            array_push($products, $product);
        }        $this->set('products',$products);
    }

    public function products() {
        if (isset($_POST['search'])) {
            $search = preg_replace("/[^\_\.\+\-\@ a-zA-Z0-9]/", '', $_POST['search']);
            $rawProducts = $this->_model->getProductsByTitle($search);
            if (empty($rawProducts)) {
                $this->set('message', "Geen producten gevonden voor: ".$_POST['search']);
            }
        } else {
            $rawProducts = $this->_model->getProducts();
        }

        $products = [];
        foreach ($rawProducts as $value) {
            $platformIds = explode(',', $value['']['Platforms']);
            $platforms = [];
            foreach ($platformIds as $platformId) {                
                $platform = new Platform($this->_model->getPlatformById($platformId));
                array_push($platforms, $platform);
            }
            $product = new Product($value);
            $product->setPlatforms($platforms);
            array_push($products, $product);
        }
        $this->set('products',$products);
    }
    
    public function product($productId = 1) {
        $rawProduct = $this->_model->getProductById($productId);
        
        $platformIds = explode(',', $rawProduct['']['Platforms']);
        $platforms = [];
        foreach ($platformIds as $platformId) {                
            $platform = new Platform($this->_model->getPlatformById($platformId));
            array_push($platforms, $platform);
        }
        $product = new Product($rawProduct);
        $product->setPlatforms($platforms);

        $this->set('product',$product);
    }

    public function register() {
        $message = '';
        if (isset($_POST['submit'])) {
            $error = false;
            $user = [];

            // validations
            $password1 = $_POST['register']['password'][1];
            $password2 = $_POST['register']['password'][2];

            if (!empty($password1) && !empty($password2)) {
                if ($password1 === $password2) {
                    $user['password'] = preg_replace("/[^A-Za-z0-9 ]/", '', $password1);
                } else {
                    $error = true;
                    $message = 'Wachtwoorden komen niet overeen';
                }
            } else {
                $error = true;
                $message = 'Wachtwoord niet ingevuld';
            }

            foreach ($_POST['register'] as $key => $value) {
                // Skip password
                if ($key === 'password') {
                    continue;
                }

                if (!empty($value) || $key === 'prefix') {
                    $user[$key] = preg_replace("/[^\_\.\+\-\@ a-zA-Z0-9]/", '', $value);
                } else {
                    $error = true;
                    $message = 'Niet alle velden zijn ingevuld.';
                }
            }


            if (!$error) {
                $result = $this->_model->checkEmail($user['email']);
                if (!$result) {
                    $this->_model->insertUser($user);
                    $message = 'Bedankt voor het aanmelden';
                } else {
                    $message = 'E-mailadres al in gebruik';
                }
            }
      }

      $this->set('message', $message);
    }
    
    public function login() {
      $message = '';
      if(isset($_POST['submit'])){
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
          $user = $this->_model->checkUser($_POST['email'],$_POST['password']);
          if ($user) {
            $_SESSION['userID'] = $user[0]['User']['id'];
            $_SESSION['userRole'] = $user[0]['User']['role'];
            header("Location:/users/homepage");
          } else {
            $message = "Inloggegevens incorrect";
          }
        } else {
          $message = "E-mailadres of wachtwoord niet ingevuld";
        }
      }
      
      $this->set('message',$message);
    }
  }