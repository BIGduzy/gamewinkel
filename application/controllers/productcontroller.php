<?php
class ProductController {
	private $products;

	public function __construct($products) {
		$this->products = [];
		for ($i = 0; $i < count($products); $i++) {
			$this->products[$i] = new Product($products[$i]);
		}
	}

	public function getProducts() {
		return $this->products;
	}
}