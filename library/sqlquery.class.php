<?php
 class SqlQuery
 {
	//Fields
	protected $_dbHandle;
	protected $_result;
	
	public function connect($host, $user, $password, $dbname)	{
		$this->_dbHandle = mysqli_connect($host, $user, $password,$dbname) or die(mysql_error());
		if ($this->_dbHandle) {
			return true;
		} else {
			return false;
		}
	}

	public function query($query, $singleResult = 0) {
		$this->_result = mysqli_query($this->_dbHandle, $query);
    
		if (preg_match("/select/i", $query)) {
			$result 	= array();
			$table 		= array();
			$field 		= array();
			$tempResults = array();
			$numOfFields = mysqli_num_fields($this->_result);
			
			for ($i = 0; $i < $numOfFields; $i++) {
				array_push($table, mysqli_fetch_field_direct($this->_result, $i)->table);
				array_push($field, mysqli_fetch_field_direct($this->_result, $i)->name);
			}
      
			while($row = mysqli_fetch_row($this->_result)) {
				for ($i = 0; $i < $numOfFields; $i++) {
					$table[$i] = trim(ucfirst($table[$i]), 's');
					$tempResults[$table[$i]][$field[$i]] = $row[$i];
				}
				
				if ($singleResult == 1)	{
					mysqli_free_result($this->_result);
					return $tempResults;
				}
				array_push($result, $tempResults);
			}
			mysqli_free_result($this->_result);
			return $result;			
		}
	}

	public function find_last_inserted_id()	{
		return mysqli_insert_id($this->_dbHandle);
	}
 }
?>














